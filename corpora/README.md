# Corpora for Grew-match instances

 * All corpora for all Grew-match instances are declared here (the same corpus can be used in more than one instance, like Naija in universal and in naija instances)
 * tuto declares one of the corpus used elsewhere but with a different id (each corpus id should be different in a given instance)
